import csv
#from sklearn import svm
import numpy as np
import matplotlib.pyplot as plt

training = []
labels = []
values = []

with open('dataset.txt', 'rb') as dataset:
	plainData = csv.reader(dataset, delimiter=',')
	for row in plainData:
		try:
			numero, data, sorteados = tuple(row)
			training.append(row)
			labels.append(numero)
			for value in sorteados.strip().split(' '):
				values.append(int(value))
		except Exception as e:
			print e

try:
	plt.hist(values, 60)
	plt.show()
except Exception as e:
	print e

#clf = svm.SVC()
#clf.fit(training, labels)